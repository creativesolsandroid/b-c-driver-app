package com.cs.bcdriver;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CS on 12-08-2016.
 */
public class StoreDetails extends AppCompatActivity {

    TextView navUser, viewUser, storeName, storeAddress, orderTakenTxt, orderNumber, mstore_to_userloc_detail_hours;
    LinearLayout navStore, call, message, orderTaken;
    Order order;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    GPSTracker gps;
    public static Double lat, longi;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 4;
    SharedPreferences userPrefs;
    String userId, response;

    SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
    SimpleDateFormat timeFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);
    SimpleDateFormat timeFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
    SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
    private String timeResponse = null;

    private Timer timer = new Timer();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.store_details);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        order = (Order) getIntent().getSerializableExtra("order_obj");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navUser = (TextView) findViewById(R.id.navigate_user);
        viewUser = (TextView) findViewById(R.id.view_user);
        navStore = (LinearLayout) findViewById(R.id.navigate_store);
        storeName = (TextView) findViewById(R.id.store_detail_name);
        storeAddress = (TextView) findViewById(R.id.store_detail_address);
//        google_map = findViewById(R.id.google_map);
        call = (LinearLayout) findViewById(R.id.store_detail_call);
        message = (LinearLayout) findViewById(R.id.store_detail_message);
        orderTaken = (LinearLayout) findViewById(R.id.order_taken);
        orderTakenTxt = (TextView) findViewById(R.id.order_taken_txt);
        orderNumber = (TextView) findViewById(R.id.order_number);
        mstore_to_userloc_detail_hours = findViewById(R.id.store_to_userloc_detail_hours);


        storeName.setText(order.getStoreName());
        storeAddress.setText(WordUtils.capitalizeFully(order.getStoreAddress()));
        orderNumber.setText("#"+order.getInvoiceNo());

        navUser.setVisibility(View.GONE);
        viewUser.setVisibility(View.GONE);

        navUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+order.getUserLat()+","+order.getUserLong());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });

//        google_map.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getGPSCoordinates();
//
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+lat+","+longi+"&daddr="+order.userLat+","+order.userLong+""));
//                startActivity(intent);
//            }
//        });


        viewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StoreDetails.this, CustomerDetails.class);
                i.putExtra("order_obj", order);
                startActivity(i);

            }
        });

        navStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+order.getStoreLat()+","+order.getStoreLong());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + order.getStoreMobile()));
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + order.getStoreMobile()));
                    startActivity(intent);
                }
            }
        });

        orderTaken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdateOrderStatus().execute(Constants.live_url+"/api/DriverApp/GetUpdateOrderStatus?DriverId="+userId+"&OrderId="+order.getOrderId()+"&OrderStatus=OnTheWay&Comment=");

            }
        });

        new GetCurrentTime().execute();

        timer.schedule(new MyTimerTask(), 2000, 60000);

    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int currentapiVersion = Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {
                        if (!canAccessLocation()) {
                            requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                        } else {
                            getGPSCoordinates();
                        }
                    } else {
                        getGPSCoordinates();
                    }
//                    getGPSCoordinates();
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void getGPSCoordinates() {
        gps = new GPSTracker(StoreDetails.this);
        JSONObject parent = new JSONObject();
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
//                lat = 24.805712;
//                longi = 46.693132;

                try {

                    JSONArray mainItem = new JSONArray();

                    JSONObject mainObj = new JSONObject();
                    mainObj.put("Latitude", lat);
                    mainObj.put("Longitude", longi);
                    mainObj.put("DriverId", userId);
                    mainObj.put("IsActive", true);
//                    mainObj.put("OnlineStatus", true);

                    mainItem.put(mainObj);

                    parent.put("DriverLoc", mainItem);
                } catch (JSONException je) {
                    je.printStackTrace();
                }
//                new UpdateLocationToServer().execute("http://85.194.94.241/oreganoservices/api/DriverApp?Latitude="+lat+"&Longitude="+longi+"&driverid="+userId);

                new UpdateLocationToServer().execute(parent.toString());
                Log.i("parent TAG", "" + parent.toString());
                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(StoreDetails.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                } else {
                    Toast.makeText(StoreDetails.this, "Location permission denied", Toast.LENGTH_LONG).show();
                }
                break;
            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + order.getStoreMobile()));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(StoreDetails.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//
//        switch (requestCode) {
//
//
//
//        }
//    }

    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetails.this);
            dialog = ProgressDialog.show(StoreDetails.this, "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("Responce", "" + serverTime);
            }else{
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(serverTime == null){
                dialog.dismiss();
            } else if(serverTime.equals("no internet")){
                dialog.dismiss();
                Toast.makeText(StoreDetails.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
                }catch (JSONException je){
                    je.printStackTrace();
                }
                new getTrafficTime().execute();

            }


            super.onPostExecute(result1);
        }
    }

    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        String distanceResponse;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetails.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + order.getStoreLat() + "," + order.getStoreLong() +"&destinations="+ order.getUserLat() +","+ order.getUserLong()+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response1: " + distanceResponse);
                return distanceResponse;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(StoreDetails.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        String value = jo3.getString("value");
//                        if(language.equalsIgnoreCase("En")) {
                        mstore_to_userloc_detail_hours.setText(""+secs);
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            travelTimeText.setText(secs+ "  وقت السفر" );
//                        }
                        Date current24Date = null, currentServerDate = null;
                        Date expectedTimeDate = null, expectedTime24 = null;
                        try {
                            current24Date = timeFormat.parse(timeResponse);
                            expectedTime24 = timeFormat2.parse(order.getExpectedTime());
                            Log.e("TAG","response"+timeResponse);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String currentTime =timeFormat1.format(current24Date);
                        String expTimeStr = timeFormat1.format(expectedTime24);
                        try {
                            currentServerDate = timeFormat1.parse(currentTime);
                            expectedTimeDate = timeFormat1.parse(expTimeStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        long diff = expectedTimeDate.getTime() - currentServerDate.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        int expMins = (int) diffMinutes * 60 * 1000;
                        Log.i("TAG", "mins response: " + expMins);
                        int mins = (Integer.parseInt(value)/60)+1;
                        Calendar now = Calendar.getInstance();
                        now.setTime(currentServerDate);
                        now.add(Calendar.MINUTE, mins);
                        currentServerDate = now.getTime();
                        String CTimeString = timeFormat1.format(currentServerDate);
//                        mstore_to_userloc_detail_hours.setText(""+CTimeString);
                        int noOfMinutes = mins * 60 * 1000;//Convert minutes into milliseconds

//                        startTimer(noOfMinutes);//start countdown
//                        startTimer1(expMins);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }

    public class UpdateOrderStatus extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetails.this);
            dialog = ProgressDialog.show(StoreDetails.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(StoreDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(StoreDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            if(result.contains("Failure")){
                                JSONObject jo= new JSONObject(result);
                                String msg = jo.getString("Failure");
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(StoreDetails.this, android.R.style.Theme_Material_Light_Dialog));

                                    // set title
                                    alertDialogBuilder.setTitle("Bakery & Co");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(msg)
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }else{
                                Intent i = new Intent(StoreDetails.this, CustomerDetails.class);
                                i.putExtra("order_obj", order);
                                startActivity(i);
                                finish();
                            }






                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(StoreDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class UpdateLocationToServer extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
//        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetails.this);
//            dialog = ProgressDialog.show(MainActivity.this, "",
//                    "Registering...");


        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.live_url + "/api/DriverApp/UpdateDriverLocation");


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(StoreDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
